<!-- .slide: data-background-image="images/gitlab-icon-rgb.svg" data-background-opacity="0.5" data-background-size="contain" data-background-transition="slide" -->

# GitLab em
# 10 Minutos

InterNorte 2019  
Redenção-PA

----  ----

## Whoami

- **Tiago Rocha**
- Engenheiro de Operações
- Gerente de Rede na [Prefeitura de Parauapebas](https://www.parauapebas.pa.gov.br/)
- Membro do Grupo [DevOpsPBS](https://gitlab.com/devopspbs)

----  ----

## O que é GitLab?

GitLab é uma plataforma completa de DevOps, entregue como uma única aplicação.

<a href="https://about.gitlab.com/devops/"><img src="images/devops-loop.svg" alt="DevOps"></a>

----

<!-- .slide: data-background-image="images/project-view.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

<a href="https://git-scm.com/"><img height="150" src="images/Git-logo.svg" alt="Git"></a>

GitLab é um gerenciador de repositório de software baseado em **Git**.

----

### As Opções

- **Enterprise Edition**
  - Versão para uso corporativo. É paga e conta com suporte da GitLab Inc.
- **Community Edition**
  - Versão Software Livre. Indicada para quem quer rodar aplicações livres.
- [**gitlab.com**](https://about.gitlab.com/)
  - Serviço de nuvem com direito a conta gratuita.

----  ----

## Recursos

Vejamos alguns recursos e integrações do GitLab que valem a pena conhecer e experimentar.

----

<!-- .slide: data-background-image="images/web-ide-cover.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## Web IDE

----

<!-- .slide: data-background-image="images/gitlab-pages-examples.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## GitLab Pages

----

<!-- .slide: data-background-image="images/issue-board.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## Issue e Kanban Board

----

<!-- .slide: data-background-image="images/roadmap_view.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## ToDo List e RoadMap

----

<!-- .slide: data-background-image="images/diff-view.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## Merge Request

----

<!-- .slide: data-background-image="images/pipeline-graph.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## CI/CD

----

<!-- .slide: data-background-image="images/instance-level-clusters.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## Kubernetes Integration

----

<!-- .slide: data-background-image="images/embed_metrics.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

## Monitoramento

----  ----

<!-- .slide: data-background-image="images/gitlab-contribute-new-orleans-2019-team-photo.jpg" data-background-opacity="0.2" data-background-size="contain" data-background-transition="slide" -->

## Comunidade

**Grupos no Gitter**  
<a href="https://gitter.im/gitlabhq/gitlabhq"><img height="100" src="images/gitter-logo.svg" alt="Gitter"></a>

**Grupos no Telegram**  
<a href="https://t.me/gitlabbr"><img height="100" src="images/Telegram_logo.svg" alt="Telegram"></a>

----

<!-- .slide: data-background-image="images/gitlab-fan-day.png" data-background-opacity="0.2" data-background-size="contain" data-background-transition="slide" -->

## GitLab Fan Club

https://gitlabfan.com/

GitLab não têm só usuários.  
GitLab têm verdadeiros **fãs**.

----

<!-- .slide: data-background-image="images/gitlab-heroes-icon.svg" data-background-opacity="0.3" data-background-size="contain" data-background-transition="slide" -->

## GitLab Heroes

O GitLab Heroes envolve, apoia e reconhece membros da comunidade GitLab em geral, que fazem contribuições excelentes ao GitLab e à nossa comunidade em todo o mundo.

https://about.gitlab.com/community/heroes/

----  ----

## Conclusão

----  ----

## Obrigado!

----  ----

## Contatos

- **Blog:** [tiagorocha.eti.br](https://tiagorocha.eti.br/)
- **E-mail:** tiagorocha at disroot.org
- **Telegram:** [@Tiag0Rocha](https://t.me/Tiag0Rocha)
- **Riot/Matrix:** tiagorocha
- **IRC:** tiagorocha (Freenode e OFTC)
