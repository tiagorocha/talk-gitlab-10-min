FROM node:11.10-stretch

RUN apt -qqy update && \
    apt -qqy install auto-apt-proxy && \
    apt -qqy install pandoc git && \
    rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/hakimel/reveal.js.git && \
    cd reveal.js && \
    npm install

WORKDIR /reveal.js
VOLUME /reveal.js

EXPOSE 8000

CMD [ "npm", "start" ]
