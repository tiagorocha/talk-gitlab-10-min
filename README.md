# GitLab em 10 Minutos

Slides da apresentação [GitLab em 10 Minutos](https://tiagorocha.gitlab.io/talk-gitlab-10-min/) para o [InterNorte 2019](https://2019.internorte.com.br/).

Essa apresentação foi construída usando [reveal.js](https://github.com/hakimel/reveal.js) com [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/) e hospedada no [GitLab Pages](https://about.gitlab.com/product/pages/).

## Licença

GFDL 1.3 or any later version
